import requests
from bs4 import BeautifulSoup
from random import choice
from csv import DictReader

base_url="http://quotes.toscrape.com"

def read_quotes(filename):
    with open(filename, "r") as file:
        csv_reader = DictReader(file)
        return list(csv_reader)

def start_game(quotes):
    quote = choice(quotes)
    chances = 5
    print("Here is a quote: ",quote["text"])
    guess = ''
    while guess.lower() != quote["author"].lower() and chances:
        guess = input(f"Who said this quote? Guesses remaining: {chances} \n")
        if guess.lower() == quote['author'].lower():
            print("You got it right")
            break
        chances -= 1
        if chances == 4:
            res = requests.get(f"{base_url}{quote['bio_link']}")
            soup = BeautifulSoup(res.text, "html.parser")
            birth_date = soup.find(class_="author-born-date").get_text()
            birth_place = soup.find(class_="author-born-location").get_text()
            print(f"Here's a hint: The author was born in {birth_date} {birth_place}")
        if chances == 3:
            full_name = quote["author"].split(" ")
            fn = len(full_name[0])
            ln = len(full_name[1])
            print(f"Here's a hint: first name has {fn} letters, last name has {ln} letters")
        elif chances == 2:
            print(f"Here's a hint: The author's first name starts with {quote['author'][0]}")
        elif chances == 1:
            last_initial = full_name[1][0]
            print(f"Here's a hint: The author's last name starts with {last_initial}")
        elif chances ==0:
            print(f"Sorry you ran out of guesses. The answer was {quote['author']} ")

    again = ''
    while again.lower() not in ('y','yes','n','no'):
        again = input("Would you like to play again (y/n)?")
    if again.lower() in ('yes','y'):
        return start_game(quotes)
    else:
        print("OK, goodbye!")

quotes = read_quotes("quotes.csv")
start_game(quotes)


